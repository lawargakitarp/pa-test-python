__author__ = 'kitarp'
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import gc
import seaborn as sns
import sklearn.cross_validation as cv
from sklearn.linear_model import LogisticRegression
from sklearn.lda import LDA
from sklearn.neighbors import KNeighborsClassifier
from sklearn.grid_search import GridSearchCV
import sklearn.metrics as mt
import pickle
from sklearn.externals import joblib


def plot_confusion_matrix(cm, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(2)
    plt.xticks(tick_marks, [0,1], rotation=45)
    plt.yticks(tick_marks, [0,1])
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def AccuracyCalc(y, y_pred):
    return round(mt.accuracy_score(y, y_pred),3)

def ClassificationReport(y, y_pred):
    return mt.classification_report(y, y_pred, labels=[0,1], target_names=["crime_low", "crime_high"])

def ModelScorer(pred_train, y_train, pred_test, y_test):
    accuracy_train = AccuracyCalc(y_train, pred_train)
    accuracy_test = AccuracyCalc(y_test, pred_test)
    cr_train = ClassificationReport(y_train, pred_train)
    cr_test = ClassificationReport(y_test, pred_test)
    return accuracy_train, accuracy_test, cr_train, cr_test

def ModelBuilderLogit(X_train,y_train, X_test, config):
    model_logit = GridSearchCV(LogisticRegression(),param_grid=config["params"]["logit"],cv=config["cv"])
    model_logit.fit(X_train,y_train)
    return model_logit, model_logit.predict(X_train), model_logit.predict(X_test)

def ModelBuilderLDA(X_train,y_train,X_test, config):
    model_LDA = LDA()#RandomizedSearchCV(LDA(),param_grid=config["params"]["lda"],cv=config["cv"])
    model_LDA.fit(X_train,y_train)
    return model_LDA, model_LDA.predict(X_train), model_LDA.predict(X_test)

def ModelBuilderKNN(X_train,y_train,X_test,config):
    model_KNN = GridSearchCV(KNeighborsClassifier(),param_grid=config["params"]["knn"],cv=config["cv"])
    model_KNN.fit(X_train,y_train)
    return model_KNN, model_KNN.predict(X_train), model_KNN.predict(X_test)

def ModelComparator(X,y, config):
    X_train, X_test, y_train, y_test = cv.train_test_split(X,y, test_size=0.2, random_state=1)
    pred_train={}
    pred_test={}
    accuracy_train={}
    accuracy_test={}
    cr_train={}
    cr_test={}
    for model in config["models"]:
        if "logit" in model:
            model_obj, pred_train[model], pred_test[model]=ModelBuilderLogit(X_train,y_train, X_test, config)
        if "lda" in model:
            model_obj, pred_train[model], pred_test[model]=ModelBuilderLDA(X_train,y_train, X_test, config)
        if "knn" in model:
            model_obj, pred_train[model], pred_test[model]=ModelBuilderKNN(X_train,y_train, X_test, config)
        accuracy_train[model], accuracy_test[model], cr_train[model], cr_test[model] = ModelScorer(pred_train[model], y_train, pred_test[model], y_test)
        cm = mt.confusion_matrix(y_test, pred_test[model])
        cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

        joblib.dump(model_obj,'output/models/%s.pkl'%model)
        plt.figure()
        plot_confusion_matrix(cm_normalized, title='Normalized confusion matrix')
        plt.savefig('output/confusion_matrix_%s.jpg'%model)
    return accuracy_train, accuracy_test, cr_train, cr_test

def run_models(X,y,config):
    accuracy_train, accuracy_test, cr_train, cr_test = ModelComparator(X,y,config)

    for model in config["models"]:
        print model,accuracy_test[model]

    for model in config["models"]:
        print model,cr_test[model]


def main():

    config={"models":["logit","lda","knn"],
        "params":{"logit":{"C":[0.0001,0.001,0.01,0.1,1.0,10],
                           "tol":[0.0001,0.001,0.01,0.1,1]},
                  "lda":{"tol":[0.0001,0.001,0.01,0.1,1,10]},
                  "knn":{"n_neighbors":[1,3,5,7,10,25,100]}},
        "cv":10}
    accuracy_train={}
    accuracy_test={}
    cr_train={}
    cr_test={}
    print "Reading File"
    df= pd.read_csv('data/Boston.csv')
    print df.head()
    crim_med = np.median(df.crim)
    y = (df.crim>crim_med).astype(int)
    accuracy_train, accuracy_test, cr_train, cr_test = ModelComparator(df[['medv','lstat','age','rm']],y,config)

    for model in config["models"]:
        print model,accuracy_test[model]

    for model in config["models"]:
        print model,cr_test[model]






if __name__ == '__main__':
    main()
